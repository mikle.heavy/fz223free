<?php

namespace app\components;

use app\helpers\SimpleXMLReader;


class Fz223XmlReader extends SimpleXMLReader
{

    private $_result = [];

    public function __construct()
    {
        $this->registerCallback("ns2:item", array($this, "callbackItem"));
    }

    /**
     * Коллбэк, который разбирает XML дерево
     * Эта реализация позволяет парсить очень большие файлы, вплоть до пары Gb
     *
     * @param Fz223XmlReader $reader
     * @return bool
     */
    protected function callbackItem(Fz223XmlReader $reader): bool
    {
        $domDocument = $reader->expandDomDocument();

        /** @var \DOMElement $node */
        foreach ($domDocument->childNodes as $node) {

            $obj = new \stdClass();
            $obj->guid = $this->_sanitize('guid', $node);
            $obj->version = $this->_sanitize('version', $node);
            $obj->fullName = $this->_sanitize('fullName', $node);
            $obj->registrationNumber = $this->_sanitize('registrationNumber', $node);
            $obj->legalAddress = $this->_sanitize('legalAddress', $node);
            $obj->ogrn = $this->_sanitize('ogrn', $node);
            $obj->kpp = $this->_sanitize('kpp', $node);
            $obj->email = $this->_sanitize('email', $node);

            $result = new \stdClass();
            $result->inn = $this->_sanitize('inn', $node);
            $result->metadata = $obj;

            $this->_result[] = $result;
        }
        return true;
    }

    /**
     * Чистим данные
     *
     * @param string $nodeName
     * @param \DOMElement $node
     * @return string
     */
    protected function _sanitize(string $nodeName, \DOMElement $node): string
    {
        $nodeName = trim($nodeName);
        if ($node->getElementsByTagName($nodeName)->item(0) === null) {
            return "";
        }
        return $node->getElementsByTagName($nodeName)->item(0)->nodeValue;
    }

    /**
     * Геттер для получения результатов разбора файла
     *
     * @return array
     */
    public function getResult(): array
    {
        return $this->_result;
    }
}