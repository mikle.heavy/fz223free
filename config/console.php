<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'app\commands',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
        '@tests' => '@app/tests',
    ],
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                    'logVars' => []
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['warning', 'error'],
                    'categories' => ['ftp-error'],
                    'logVars' => [],
                    'logFile' => '@runtime/logs/parsing-error.log',
                    'maxFileSize' => 1024,
                    'maxLogFiles' => 2,
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['info'],
                    'categories' => ['parsing'],
                    'logVars' => [],
                    'logFile' => '@runtime/logs/parsing.log',
                    'maxFileSize' => 1024,
                    'maxLogFiles' => 2,
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['warning', 'error'],
                    'categories' => ['parsing-error'],
                    'logVars' => [],
                    'logFile' => '@runtime/logs/parsing-error.log',
                    'maxFileSize' => 1024,
                    'maxLogFiles' => 2,
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['info'],
                    'categories' => ['import'],
                    'logVars' => [],
                    'logFile' => '@runtime/logs/import.log',
                    'maxFileSize' => 1024,
                    'maxLogFiles' => 2,
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['warning', 'error'],
                    'categories' => ['import'],
                    'logVars' => [],
                    'logFile' => '@runtime/logs/import-error.log',
                    'maxFileSize' => 1024,
                    'maxLogFiles' => 2,
                ],
            ],
        ],
        'ftp' => [
            'class' => '\gftp\FtpComponent',
            'connectionString' => 'ftp://fz223free:fz223free@ftp.zakupki.gov.ru/out/nsi/customerRegistry/daily:21',
            'driverOptions' =>  [
                'timeout' => 30,
                'passive' => true
            ]
        ],
        'db' => $db,
    ],
    'params' => $params,
    /*
    'controllerMap' => [
        'fixture' => [ // Fixture generation command line.
            'class' => 'yii\faker\FixtureController',
        ],
    ],
    */
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
