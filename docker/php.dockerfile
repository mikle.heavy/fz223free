FROM php:7.4-fpm-alpine

ADD ./php/www.conf /usr/local/etc/php-fpm.d/www.conf

RUN addgroup -g 1000 deploy && deploy -G deploy -g deploy -s /bin/sh -D deploy

RUN mkdir -p /var/www/html

RUN chown deploy:deploy /var/www/fz223

WORKDIR /var/www/fz223

RUN docker-php-ext-install pdo pdo_mysql ftp dom mbstring simplexml
