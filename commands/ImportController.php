<?php

namespace app\commands;


use Yii;

use app\models\Company;
use app\models\CompanyMetadata;

use yii\console\Controller;
use yii\console\Exception;
use yii\console\ExitCode;

use yii\helpers\Console;
use yii\helpers\FileHelper;
use app\helpers\FtpHelper;
use app\helpers\ZipHelper;

use app\components\Fz223XmlReader;

use gftp\FtpComponent;
use gftp\FtpException;

class ImportController extends Controller
{
    const ACTUAL_FILES_NUMBER = 15;
    const LOG_IMPORT = "import";
    const LOG_PARSING = "parsing";
    const LOG_FTP = 'ftp';


    /**
     * Действие по умолчанию
     *
     * @return void
     */
    public function actionIndex()
    {
        echo 'yii import/get-list [lastItems]' . PHP_EOL;
        echo 'yii import/remove-company INN' . PHP_EOL;
        echo 'yii import/purge' . PHP_EOL;
    }

    /**
     * Забирает с сервера все файлы, сортирует их на основании даты в названии файла, берет первые $lastItems файлы,
     * разбирает их как XML и пишет в БД
     *
     * @param int $lastItems
     * @return int
     * @throws \gftp\FtpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionGetList(int $lastItems = self::ACTUAL_FILES_NUMBER)
    {
        /** @var FtpComponent $gftp */
        $gftp = Yii::$app->ftp;
        $gftp->chdir('/out/nsi/customerRegistry/daily');
        $fileList = $gftp->ls();

        if (empty($fileList)) {
            return ExitCode::DATAERR;
        }

        $filesWithDate = [];
        foreach ($fileList as $file) {
            $filesWithDate[$file->filename] = FtpHelper::getRawFileDate($file->filename);
        }

        /**
         * Оперируя массивом с перечнем имен файлов явно определяем какие файлы самые свежие и получаем первые 15 штук
         */
        $flipped = array_flip($filesWithDate);
        krsort($flipped, SORT_NUMERIC);
        $actual = array_slice($flipped, 0, $lastItems);

        foreach ($actual as $item) {
            // забираем файл с ftp
            $localXMLFile = $this->_getFile($item);
            // парсим и пишем в БД
            $this->_parse($localXMLFile);
        }

        return ExitCode::OK;
    }

    /**
     * Позволяет удалить данные о конкретной компании по ее INN
     * Удобно для тестирования
     *
     * @param string $inn
     * @return int
     */
    public function actionRemoveCompany(string $inn)
    {
        Company::deleteAll(['inn' => $inn]);

        return ExitCode::OK;
    }

    /**
     * Позволяет удалить все данные о компаниях
     * Удобно для тестирования
     *
     * @return int
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionPurge()
    {
        Company::deleteAll();

        return ExitCode::OK;
    }

    /**
     *  приватный метод парсинга XML файла. Реализован как коллбэк.
     *
     * @param string $itemName
     * @throws Exception
     * @throws \yii\db\Exception
     */
    private function _parse(string $itemName)
    {
        $reader = new Fz223XmlReader();
        if ($reader->open($itemName) === false) {
            $this->_redLog('Unable to open file as XML ' . $itemName, self::LOG_PARSING);
        }
        try{
            $reader->parse();
        } catch (\Exception $e){
            $this->_redLog('Unable to parse XML file ' . $itemName. ' reason: '.$e->getMessage(), self::LOG_PARSING);
        }

        $reader->close();
        $metadata = $reader->getResult();
        foreach ($metadata as $data) {
            $company = Company::getByInn($data->inn);
            if (!$company) {
                $company = new Company();
                $company->inn = $data->inn;
                if ($company->validate()) {
                    $company->save(false);
                } else {
                    $this->_redLog('Не удалось сохранить данные о компании' . $data->inn, self::LOG_IMPORT);
                }
            }

            if (isset($data->metadata->guid)) {
                $companyMetadata = CompanyMetadata::getByGuid($data->metadata->guid);
                if ($companyMetadata === null) {
                    $companyMetadata = new CompanyMetadata();
                    $companyMetadata->inn = $data->inn;
                    $companyMetadata->version = $data->metadata->version;
                    $companyMetadata->guid = $data->metadata->guid;
                    $companyMetadata->full_name = $data->metadata->fullName;
                    $companyMetadata->legal_address = $data->metadata->legalAddress;
                    $companyMetadata->registration_number = $data->metadata->registrationNumber;
                    $companyMetadata->ogrn = $data->metadata->ogrn;
                    $companyMetadata->kpp = $data->metadata->kpp;
                    $companyMetadata->email = $data->metadata->email;
                    if ($companyMetadata->validate()) {
                        $companyMetadata->save(false);
                    } else {
                        $this->_redLog('Не удалось сохранить метаданные о компании' . $data->inn, self::LOG_IMPORT);
                    }

                    $date = new \DateTime();
                    $now = $date->getTimestamp();
                    $company = Company::getByInn($data->inn);
                    $company->last_attempt_at = $now;
                    $company->save();

                }
            }
        }
    }

    /**
     * Забирает архив с ftp, достает файл из архива и проверят, что это XML
     *
     * @param string $filename
     * @return string
     * @throws Exception
     * @throws \yii\base\InvalidConfigException
     */
    private function _getFile(string $filename)
    {
        /** @var FtpComponent $gftp */
        $gftp = Yii::$app->ftp;
        $zippedLocalFile = "/tmp/" . $filename;
        // get da file
        try {
            $gftp->get($filename, $zippedLocalFile, FTP_BINARY);
        } catch (FtpException $e) {
            $this->_redLog('Can not download file ' . $filename, self::LOG_FTP);
        } catch (yii\base\ErrorException $e) {
            throw new Exception($e->getMessage());
        }

        if (!is_readable($zippedLocalFile)) {
            $this->_redLog('Can not save file ' . $zippedLocalFile, self::LOG_FTP);
        }

        $unzippedLocalFile = "/tmp/" . pathinfo($zippedLocalFile)['filename'];
        // unzip to /tmp dir
        if (ZipHelper::unzipFile($zippedLocalFile, "/tmp") === false) {
            $this->_redLog('Can not unzip downloaded file ' . $zippedLocalFile, self::LOG_FTP);
        }

        if (!is_readable($unzippedLocalFile)) {
            $this->_redLog('Can not read downloaded XML file: ' . $unzippedLocalFile, self::LOG_PARSING);
        }

        $mimeType = FileHelper::getMimeType($unzippedLocalFile);
        if (!isset($mimeType) || $mimeType !== "text/xml") {
            $this->_redLog('Downloaded file probably not an XML file: ' . $unzippedLocalFile, self::LOG_PARSING);
        }

        return $unzippedLocalFile;
    }

    /**
     * Красный _log
     *
     * @param string $msg Сообщение
     * @param string $category категория ошибки
     * @param bool   $eol Выводить перевод строки/возврат каретки
     *
     * @return void
     */
    private function _redLog(string $msg, string $category, bool $eol = true)
    {
        $this->stdout($msg, Console::FG_RED, Console::BOLD);

        $oldTraceLevel = Yii::$app->log->traceLevel;
        Yii::$app->log->traceLevel = 0;

        Yii::error($msg, $category);

        Yii::$app->log->traceLevel = $oldTraceLevel;

        if ($eol) {
            echo PHP_EOL;
        }
    }
}