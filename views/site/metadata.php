<?php

/**
 * @var $this yii\web\View
 * @var $searchModel app\models\CompanyMetadataSearch
 * @var $inn string
 */


/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Список метаданных компании с ИНН '.$inn;

$this->params['breadcrumbs'][] = ['label' => 'Список метаданных компаний', 'url' => ['/']];

?>
<div class="site-index">

    <div class="company-index-index">

        <h1><?= Html::encode($this->title); ?></h1>

        <?= GridView::widget(['dataProvider' => $dataProvider,
            //'filterModel' => $searchModel,
            'columns' => [
                'guid', 'registration_number', 'version', 'full_name', 'ogrn', 'kpp', 'legal_address', 'email',
                 ['label' => 'Дата сохранения',
                    'value' => function ($model)
                    use ($searchModel) {
                        return Yii::$app->formatter->format($model->created_at, 'datetime');
                    },
                ],
            ],
        ]); ?>
    </div>
</div>
