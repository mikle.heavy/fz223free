<?php

/* @var $this yii\web\View */
/* @var $searchModel app\models\CompanySearch */

/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

$this->title = 'Список компаний';
?>


        <h1><?= Html::encode($this->title); ?></h1>
        <?= GridView::widget(['dataProvider' => $dataProvider,
            //'filterModel' => $searchModel,
            'columns' => [
                [
                    'label' => 'ИНН',
                    'value' => function ($model) use ($searchModel) {
                        return Html::a($model->inn, Url::toRoute(['metadata/' . $model->inn]));
                    },
                    'format' => 'raw',

                ],
                ['label' => 'Дата сохранения',
                    'value' => function ($model)
                    use ($searchModel) {
                        return Yii::$app->formatter->format($model->created_at, 'datetime');
                    },
                ],
                [
                    'label' => 'Дата дополнения данных',
                    'value' => function ($model) use ($searchModel) {
                        return Yii::$app->formatter->format($model->created_at, 'datetime');
                    },
                ],
            ],
        ]); ?>

