<?php

namespace app\helpers;

use DateTime;

class FtpHelper
{

    /**
     * @param string $filename
     * @return false|string
     */
    public static function parseFilename(string $filename)
    {
        $parts = explode("_", $filename);
        if (isset($parts[2]) && !empty($parts[2])) {
            $date = DateTime::createFromFormat('Ymd', $parts[2]);
            return $date->format('Y-m-d');
        }

        return false;
    }

    /**
     * Возвращает дату создания файла
     *
     * @param string $filename
     * @return int
     */
    public static function getRawFileDate(string $filename): int
    {
        $parts = explode("_", $filename);
        if (isset($parts[2]) && !empty($parts[2])) {
            return $parts[2];
        }
    }

    /**
     * @param DateTime $day
     * @return string
     */
    public static function constructFilename(Datetime $day): string
    {
        $rawDay = $day->format('Ymd');
        $filename = "customerRegistry_inc_" . $rawDay;
    }

}