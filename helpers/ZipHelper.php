<?php

namespace app\helpers;

/**
 * Class ZipHelper
 * @package common\helpers
 */
class ZipHelper
{
    const ZIP_UTIL = 'gzip';
    const UNZIP_UTIL = 'unzip';

    /**
     * Сжатие файла
     *
     * @param string $file Имя файла для сжатия
     *
     * @return bool Успешно ли выполнена команда
     */
    public static function zipFile($file)
    {
        $zipUtil = self::ZIP_UTIL;

        $file = escapeshellarg($file);
        $shellCommand = "{$zipUtil} {$file}";

        system($shellCommand, $exitCode);

        return ((int) $exitCode === 0);
    }

    /**
     * Распаковка файла
     *
     * @param string $file Имя файла для распаковки
     * @param string $destDir
     *
     * @return bool Успешно ли выполнена команда
     */
    public static function unzipFile(string $file, string $destDir = '/tmp')
    {
        $unzipUtil = self::UNZIP_UTIL;

        $file = escapeshellarg($file);
        $shellCommand = "{$unzipUtil} -oq {$file} -d {$destDir}";

        system($shellCommand, $exitCode);

        return ((int) $exitCode === 0);
    }
}