<?php

namespace app\helpers;

/**
 * Class LogHelper
 * @package common\helpers
 */
class LogHelper
{
    /**
     * Log info message
     *
     * @param $message
     * @param $category
     */
    public static function info($message, $category)
    {
        \Yii::info($message, $category);
        \Yii::getLogger()->flush(true);
    }

    /**
     * Log error message
     *
     * @param $message
     * @param $category
     */
    public static function error($message, $category)
    {
        \Yii::error($message, $category);
        \Yii::getLogger()->flush(true);
    }

    /**
     * Log warning message
     *
     * @param $message
     * @param $category
     */
    public static function warning($message, $category)
    {
        \Yii::warning($message, $category);
        \Yii::getLogger()->flush(true);
    }
}