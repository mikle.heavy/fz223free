<?php

use yii\db\Migration;

/**
 * Class m210324_144606_init
 */
class m210324_144606_init extends Migration
{


    public function up()
    {
        $this->createTable('{{%company}}', [
            'id' => $this->primaryKey(),
            'inn' => $this->string(12)->notNull()->unique(),
            'last_attempt_at' => $this->integer(),
            'created_at' => $this->integer()->unsigned()->notNull(),
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');

        $this->createTable('{{%company_metadata}}', [
            'id' => $this->primaryKey(),
            'guid' => $this->string(40)->unique(),
            'inn' => $this->string(12)->notNull(),
            'registration_number' => $this->double(32)->unsigned()->defaultValue(0),
            'version' => $this->integer()->unsigned()->defaultValue(0),
            'full_name' => $this->string(255)->defaultValue(""),
            'ogrn' => $this->integer()->unsigned()->defaultValue(0),
            'kpp' => $this->integer()->unsigned()->defaultValue(0),
            'legal_address' => $this->string(255)->defaultValue(""),
            'email' => $this->string(255)->defaultValue(""),
            'created_at' => $this->integer()->unsigned()->notNull(),
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');

        $this->createTable('{{%imported_files}}', [
            'id' => $this->primaryKey(),
            'file_name' => $this->string(255)->notNull(),
            'file_szie' => $this->integer()->notNull(),
            'created_at' => $this->integer()->unsigned()->notNull(),
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');

        //$this->createIndex('inn_parent', 'company_metadata', ['inn']);

        $this->addForeignKey('fk_inn_parent', 'company_metadata', 'inn',
            'company', 'inn', 'CASCADE', 'CASCADE');

        /**
        $this->addForeignKey('fk_imported_file', 'company_metadata', 'import_file',
            'imported_files', 'id', 'CASCADE', 'CASCADE');
        */
    }

    public function down()
    {
        echo "m210324_144606_init cannot be reverted.\n";

        return false;
    }

}
