<?php

namespace app\controllers;


use Yii;
use app\models\CompanyMetadataSearch;
use app\models\CompanySearch;
use yii\web\Controller;



class SiteController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Список всех компаний по их ИНН
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new CompanySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Карточка компании с историей изменений
     *
     * @param string $inn
     * @return string
     */
    public function actionMetadata(string $inn)
    {
        $searchModel = new CompanyMetadataSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('metadata', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'inn' => $inn,
        ]);
    }

}
