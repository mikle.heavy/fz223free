## тестовое задание для http://etpgpb.ru

### Автор Симоненко Михаил 

#### 25.03.2021 19:35

[ТЗ здесь](img/tz_php.odt)

### Как это работает
Для осуществления импорта данных с ftp сервера используются консольные команды

* `./yii import/get-list x` - забирает с сервера последние `x` (по умолчанию 15) файлов, разрахивирует, парсит, пишет в БД
* `./yii import/remove-company y` - удаляет из БД записи, относящиеся к компании с ИНН = `y` (удобно для проверки работы) 
* `./yii import/purge` - удаляет из БД все записи (удобно для проверки работы)

В случае возникновения ошибок в отдельных файлах при импорте процесс не прерывается, 
а в консоли отображается причина проблемы:

![ошибка в консоли](img/4.PNG)

Логирование ошибок в файлы описано в настройках

![Логирование ошибок в файлы](img/5.PNG)

### веб интерфейс

Увидеть список компаний и их метаданные с историей изменений можно в веб интерфейсе. Адресация страниц реализована 
через роутер, что дает красивый и понятный URL  

### список компаний с паджинацией

![список компаний с паджинацией](img/1.PNG)

### метаданные конкретной компании с историей изменений

![метаданные конкретной компании с историей изменений](img/2.PNG)

### детали реализации

Парсинг XML файла выполнен как вызов коллбэка, реализованного в классе `components/Fz223XmlReader`, который 
наследует реализацию `helpers/SimpleXMLReader`. Преимуществом 
такой схемы работы является возможность парсинга очень объемных файлов, размером вплоть до гигабайта.

Общение с ftp сервером реализовано в `commands/ImportController`, команды которого описаны в пункте `Как это работает`

Структура данных описана в моделях `models/Company` и `models/CompanyMetadata`. Паджинация в виджетах веб интерфейса 
описана в `models/CompanySearch` и `models/CompanyMetadataSearch`

### Схема БД

![Схема БД](img/3.PNG)

### DDL

Везде явно указана кодировка. В MySQL "из коробки" с этим бывают проблемы

Названия большинства полей говорят сами за себя, кроме `company.last_attempt_at` - это момент, когда метаданные этой 
компании были изменены. Такое может происходить при добавлении очередной версии в `company_metadata`

Значения полей `created_at` устанавливаются моделями через механизм `behaviors` (поведения)

**company**

```sql
CREATE TABLE limpopo_mikle.company (
  id int(11) NOT NULL AUTO_INCREMENT,
  inn varchar(12) NOT NULL,
  last_attempt_at int(11) DEFAULT NULL,
  created_at int(11) UNSIGNED NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 420,
AVG_ROW_LENGTH = 45,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

ALTER TABLE limpopo_mikle.company
ADD UNIQUE INDEX inn (inn);
```

**company_metadata**
```sql
CREATE TABLE limpopo_mikle.company_metadata (
id int(11) NOT NULL AUTO_INCREMENT,
guid varchar(40) DEFAULT NULL,
inn varchar(12) NOT NULL,
registration_number double UNSIGNED DEFAULT 0,
version int(11) UNSIGNED DEFAULT 0,
full_name varchar(255) DEFAULT '',
ogrn int(11) UNSIGNED DEFAULT 0,
kpp int(11) UNSIGNED DEFAULT 0,
legal_address varchar(255) DEFAULT '',
email varchar(255) DEFAULT '',
created_at int(11) UNSIGNED NOT NULL,
PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 434,
AVG_ROW_LENGTH = 531,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

ALTER TABLE limpopo_mikle.company_metadata
ADD UNIQUE INDEX guid (guid);

ALTER TABLE limpopo_mikle.company_metadata
ADD CONSTRAINT fk_inn_parent FOREIGN KEY (inn)
REFERENCES limpopo_mikle.company (inn) ON DELETE CASCADE ON UPDATE CASCADE;
```

### Требования для инсталляции

* linux
* mysql-server mysql-client
* nginx/apache
* php7.* и модули php-simplexml php-ftp php-dom php-mbstring
* unzip (либо gunzip, но тогда надо поправить параметры в `helpers/ZipHelper:46` )
