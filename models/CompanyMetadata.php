<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "company_metadata".
 *
 * @property int $id
 * @property int $inn
 * @property string $guid
 * @property int $import_file
 * @property float|null $registration_number
 * @property int|null $version
 * @property string|null $full_name
 * @property int|null $ogrn
 * @property int|null $kpp
 * @property string|null $legal_address
 * @property string|null $email
 * @property int $created_at
 *
 * @property ImportedFiles $importFile
 * @property Company $inn0
 */
class CompanyMetadata extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'company_metadata';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['inn'], 'required'],
            [['inn', 'guid'], 'string'],
            [['version', 'ogrn', 'kpp', 'created_at'], 'integer'],
            [['inn'], 'match', 'pattern' => '/^\d+$/', 'message' => 'ИНН должен состоять только из цифр'],
            [['registration_number'], 'number'],
            [['full_name', 'legal_address', 'email'], 'string', 'max' => 255],
            [['inn'], 'exist', 'skipOnError' => true, 'targetClass' => Company::class, 'targetAttribute' => ['inn' => 'inn']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'guid' => 'GUID',
            'inn' => 'ИНН',
            'registration_number' => 'Регистрационный номер',
            'version' => 'Версия',
            'full_name' => 'Полное наименование',
            'ogrn' => 'ОГРН',
            'kpp' => 'КПП',
            'legal_address' => 'Юридический адрес',
            'email' => 'Email',
            'created_at' => 'Created At',
        ];
    }


    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
            ],
        ];
    }

    /**
     * Gets query for [[ImportFile]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getImportFile()
    {
        return $this->hasOne(ImportedFiles::class, ['id' => 'import_file']);
    }

    /**
     * Возвращает метаданые компании по GUID записи
     *
     * @param string $guid
     * @return CompanyMetadata|null
     */
    public static function getByGuid(string $guid)
    {
        return self::findOne(['guid' => $guid]);
    }

        /**
     * Gets query for [[Inn0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getByInn()
    {
        return $this->hasOne(Company::class, ['inn' => 'inn']);
    }
}
