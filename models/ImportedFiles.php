<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "imported_files".
 *
 * @property int $id
 * @property string $file_name
 * @property int $file_size
 * @property int $created_at
 *
 * @property CompanyMetadata[] $companyMetadatas
 */
class ImportedFiles extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'imported_files';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['file_name', 'file_size'], 'required'],
            [['file_size', 'created_at'], 'integer'],
            [['file_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'file_name' => 'File Name',
            'file_size' => 'File Size',
            'created_at' => 'Created At',
        ];
    }


    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
            ],
        ];
    }

    /**
     * Gets query for [[CompanyMetadatas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyMetadatas()
    {
        return $this->hasMany(CompanyMetadata::class, ['import_file' => 'id']);
    }
}
