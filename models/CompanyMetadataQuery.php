<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[CompanyQuery]].
 *
 * @see CompanyMetadataQuery
 */
class CompanyMetadataQuery extends \yii\db\ActiveQuery
{

    /**
     * @inheritdoc
     * @return CompanyMetadataQuery[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return CompanyMetadataQuery|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}