<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[CompanyQuery]].
 *
 * @see CompanyQuery
 */
class CompanyQuery extends \yii\db\ActiveQuery
{

    /**
     * @inheritdoc
     * @return CompanyQuery[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return CompanyQuery|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}