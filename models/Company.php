<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "company".
 *
 * @property int $id
 * @property int $inn
 * @property int|null $last_attempt_at
 * @property int $created_at
 *
 * @property CompanyMetadata[] $companyMetadatas
 */
class Company extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'company';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['inn'], 'required'],
            [['inn'], 'string'],
            [['last_attempt_at', 'created_at'], 'integer'],
            [['inn'], 'match', 'pattern' => '/^\d+$/', 'message' => 'ИНН должен состоять только из цифр'],
            [['inn'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'inn' => 'BYY',
            'last_attempt_at' => 'Last Attempt At',
            'created_at' => 'Created At',
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
            ],
        ];
    }

    /**
     * Gets query for [[CompanyMetadata]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyAllMetadata()
    {
        return $this->hasMany(CompanyMetadata::class, ['inn' => 'inn']);
    }

    /**
     * Возвращает компанию по ее inn
     *
     * @param int $inn
     * @return Company|null
     */
    public static function getByInn(int $inn) :?Company
    {
        return self::findOne(['inn' => $inn]);
    }
}
